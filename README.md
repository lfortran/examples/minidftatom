# Minidftatom

Mini version of dftatom that still computes Schroedinger DFT energies.

## Usage

```bash
cd src
FC=lfortran cmake .
cmake --build .
```
